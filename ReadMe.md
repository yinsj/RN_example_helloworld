# Hello World
## 项目结构

### \_\_test__
测试用例代码，使用Jest框架，行为驱动测试风格

### android ios
原生项目源码和资源，主要定义了原生app的入口和通信机制
> 除了配置文件如Android的`AndroidManifest.xml`和iOS的`Info.plist`文件，基本不做修改

### node_modules
项目所依赖的包，由RN的包管理工具`yarn`或者`npm`进行管理，不直接进行操作

### index.android.js  index.ios.js
对应的iOS和安卓的入口js文件
> 倒数第二个后缀`.android`和`.ios`代表该文件的安卓和iOS版本，在针对不同平台编写特有代码时使用

### 其他
各种配置文件

## JS基础知识

### import
项目导入了一些有用的组件和API，ES6的import语法和其他语言有点区别，很多语言是整体导入，它是部分导入，也就是说可以指定一个文件内export的相关内容。

exampleFile.js:  

``` 
export default Foo = { a: 0, b: 1 }  
export let Bar = { c: 2, d: 3 }
```
anotherFile.js:  

```
import AnObject from './exampleFile'
// 1
import { Bar } from './exampleFile'   
// 2   
import * as ExampleAPI from './exampleFile'  
```

export default无需对应原变量名，其他export必须对应  
exampleFile中所有除default外的export组成一个对象  

### 对象解构

```
const Foo = { a: 0, b: 1 }
// 1
const a = Foo.a
// 2  
const { a } = Foo
```
使用对象解构语法，`{ Bar }`可理解为 `const { Bar } = *`  

### const let var 
* `const`声明一个常量
* `let`和`var`声明一个变量

### 箭头函数
**基本格式:** 箭头前为函数参数用小括号包裹，箭头后为函数体用大括号包裹；  

* 如果只有一个参数，可以省略小括号
* 如果有多个参数，用括号隔开
* 如果函数体只有一个返回语句，可以省略`return`和大括号  
* 如果返回的值为对象，需要用`()`包起来   

**特点:** 是一种更简洁效率更高的函数写法，且不绑定this/super/arguments等指针

`() => { return (<Text>Hello World</Text>) }`   
`() => (<Text>Hello World</Text>)` 

### class 
JS的class和其他语言的语法类似，但`class`其实是语法糖，是一种`function`

```
class PersonClass {
	constructor(name) {
		this.name = name
	}
	sayName() {
		console.log(this.name)
	}
}

const person = PersonClass("Alice")
person.sayName(); // outputs "Alice"
``` 

### JS中的原型链
```
function PersonType(name) { 
	this.name = name;
}     
PersonType.prototype.sayName = function() { 
	console.log(this.name); 
};   

var person = new PersonType("Alice");   
person.sayName(); // outputs "Alice"
```
JS中的`function`类型都有一个`prototype`属性  
`PersonType`函数通过`new`关键字可以生成一个该函数的实体，所有实体共享`prototype`属性  

## React Native

### AppRegistry.registerComponent()
注册app入口组件，第一个参数必须与项目名一致，第二个参数须返回一个RN组件

```
AppRegistry.registerComponent('ExampleAPP', () => app);
```

### View、Text等RN组件
组件可通过JSX语法进行组合，其属性值被传递至组件的`props`对象  

### render()
`Component`子类通过重写`render()`方法定义组件的渲染视图，该方法必须返回一个React组件树。
 > 不能为null, undefined, 非React组件或多根结点组件

### StyleSheet
通过样式表的方式组织组件的`style`属性，可以使代码更加清晰易懂，且利于样式的复用





  

