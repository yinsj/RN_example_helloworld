/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Text
} from 'react-native';

// 如果组件比较复杂，需要控制其内部状态或生命周期，则应继承Component
class app extends Component {
  // 复写此方法来定义用于渲染的视图，需返回一个React组件树
  render() {
    return (
        <View style={ styles.container }>
          <Text style={ styles.text }>
            Hello World!
          </Text>
        </View>
    );
  }
}

/*
const app = () => (
    <View style={ styles.container }>
      <Text style={ styles.text }>
        Hello World!
      </Text>
    </View>
);
*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 40,
    fontWeight: 'bold',
    color: 'blue'
  }
});

AppRegistry.registerComponent('HelloWorld', () => app);
